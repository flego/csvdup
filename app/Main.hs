module Main where

import System.Environment
import System.Exit
import System.IO

data Line = Line String

instance Show Line where
  show (Line line) = line

main :: IO ()
main = do
  putStrLn "=========================================="
  putStrLn " csvdup - a cute little .csv compare tool"
  putStrLn " v0.2 "
  putStrLn "=========================================="
  putStrLn " "
  putStrLn "Die ersten zwei angegebenen Pfade werden eingelesen..."
  paths <- getArgs
  let [csv1path, csv2path] = take 2 $ paths
  csv1 <- openFile csv1path ReadMode
  csv2 <- openFile csv2path ReadMode
  isEmpty <- checkIfEmpty csv1 csv2
  if isEmpty == True then do
    putStrLn "Mindestens eine der beiden Dateien ist leer."
    exitFailure
  else do
    putStrLn "2 Dateien gefunden."
    putStrLn " "
    putStrLn "=== Anfang der gleichen Zeilen ==="        
    putStrLn " "
    csv1pos <- hGetPosn csv1
    l2curr <- getT csv2
    ls <- lStep [] csv1pos csv1 csv2 l2curr -- [(Line "Beginn der Diffs: ")] csv1 l2 -- TODO: BETTER INIT
    -- lPrint ls
    print ls
    putStrLn "===  Ende der gleichen Zeilen  ==="
    mapM_ hClose [csv1, csv2]
    putStrLn "Dateien geschlossen."

checkIfEmpty :: Handle -> Handle -> IO Bool
checkIfEmpty csv1 csv2 = do
  p1 <- hIsEOF csv1
  p2 <- hIsEOF csv2
  return $ p1 || p2

lStep :: [Line] -> HandlePosn -> Handle -> Handle -> String -> IO ()
lStep acc csv1pos csv1 csv2 l2curr = do
  l1 <- getT csv1
  -- print $ "Matching " ++ l1 ++ " against " ++ l2curr
  let ld = lCompare l1 l2curr
  -- print ld -- DEBUG
  let la = lAppend acc ld
  det <- hIsEOF csv1
  if det then lNext la csv1pos csv1 csv2 l2curr 
  else lStep la csv1pos csv1 csv2 l2curr

lNext :: [Line] -> HandlePosn -> Handle -> Handle -> String -> IO ()
lNext acc csv1pos csv1 csv2 l2curr = do
  hSetPosn csv1pos
  det <- checkIfEmpty csv1 csv2
  if not det then do
    l2 <- getT csv2
    lStep acc csv1pos csv1 csv2 l2
  else print acc

getT :: Handle -> IO String 
getT = hGetLine

lCompare :: String -> String -> Maybe Line
lCompare l1 l2 
  | l1 == l2  = Just $ Line $ l1 ++ "\n"
  | otherwise = Nothing

lAppend :: [Line] -> Maybe Line -> [Line]
lAppend la Nothing   = la
lAppend la (Just ld) = ld : la -- FURCHTBAR INEFFIZIENT

lPrint :: [Line] -> IO ()
lPrint = mapM_ print

{-
getDiff csv1 csv2 = do
  l1 <- hGetLine csv1
  l2 <- hGetLine csv2
  if l1 == l2 then return $ Just $ putStrLn "erste gleich!" else return Nothing 
-}
