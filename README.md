# csvdup v0.2

Install by running ``ghc Main.hs -o /path/to/folder/csvdup``

Run with two file paths as arguments. csvdup will consequently 
print common lines. That is, if your RAM is large enough, of course ;)
